""" Module Doc """
from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_sockets import Sockets
from flask_cors import CORS
from .config import configure_app


APP = Flask(__name__)
sockets = Sockets(APP)
# Enable CORS
CORS(APP)
# Load Configurations
configure_app(APP)
# Configure database
DB = SQLAlchemy(APP)

ws_list = []

@sockets.route('/echo')
def echo_socket(ws):
    ws_list.append(ws)
    while not ws.closed:
        message = ws.receive()
        ws.send('BOT: {}'.format(message))


@APP.errorhandler(405)
def method_not_allowed(e):
    """ Method not allowed """
    res = jsonify(
        error='Method Not Allowed',
        message='The method is not allowed for the requested URL.',
    )
    return res, 405


@APP.errorhandler(404)
def page_not_found(e):
    """ 'Not Found """
    res = jsonify(
        error='Not Found',
        message='The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again.',
    )
    return res, 404


@APP.teardown_appcontext
def close_connection(exception):
    print('Stoping the application now is better')


from src.site.rest_controllers import SITE
from src.image.rest_controller import IMAGE

BASE_PATH = APP.config['BASE_PATH']
APP.register_blueprint(SITE, url_prefix='/{}/site/'.format(BASE_PATH))
APP.register_blueprint(IMAGE, url_prefix='/{}/image/'.format(BASE_PATH))

# Create tables
DB.create_all()
