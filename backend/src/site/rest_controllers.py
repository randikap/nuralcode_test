""" Module """
from flask import Blueprint, jsonify

SITE = Blueprint('site', __name__)


@SITE.route('/')
def index():
    """ Doc """
    return jsonify(
        username='Randika',
        email='hrandika@hotmail.com',
        id=1
    )
