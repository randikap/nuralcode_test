""" Doc module"""
import os
import logging


class BaseConfig(object):
    DEBUG = False
    TESTING = False
    # sqlite :memory: identifier is the default if no filepath is present
    # SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # SECRET_KEY = '1d94e52c-1c89-4515-b87a-f48cf3cb7f0b'
    LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    LOGGING_LOCATION = 'neuralcode.log'
    LOGGING_LEVEL = logging.DEBUG
    BASE_PATH = "api"


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    TESTING = False
    ENV = 'dev'
    HOST = 'http://127.0.0.1:5000'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///../neuralcode.db'
    UPLOAD_FOLDER="uploaded"
    # SECRET_KEY = 'a9eec0e0-23b7-4788-9a92-318347b9a39f'


class ProductionConfig(BaseConfig):
    DEBUG = False
    TESTING = False
    ENV = 'prod'
    HOST = 'http://192.168.1.251:5000'
    # SQLALCHEMY_DATABASE_URI = 'sqlite://'
    # SECRET_KEY = '8c0caeb1-6bb2-4d2d-b057-596b2dcab18e'


config = {
    "dev": "src.config.DevelopmentConfig",
    # "staging": "bookshelf.config.StagingConfig",
    "prod": "src.config.ProductionConfig",
    "default": "src.config.DevelopmentConfig"
}


def configure_app(app):
    config_name = os.getenv('FLASK_CONFIGURATION', 'default')
    app.config.from_object(config[config_name])
    app.config.from_pyfile('config.cfg', silent=True)
    # Configure logging
    handler = logging.FileHandler(app.config['LOGGING_LOCATION'])
    handler.setLevel(app.config['LOGGING_LEVEL'])
    formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)

    # Configure Security
    # user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    # app.security = Security(app, user_datastore)
    # Configure Compressing
    # Compress(app)
