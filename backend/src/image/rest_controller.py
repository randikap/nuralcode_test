""" Module """
import os
from werkzeug.utils import secure_filename
import cv2
from flask import Blueprint, send_from_directory, jsonify, request, current_app as APP
from sqlalchemy import exc
from src import DB as db
from src.image.models import Image
from .image_service import crop_image

IMAGE = Blueprint('image', __name__)
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])


@IMAGE.route('upload', methods=['POST'])
def index():
    """ Image Upload """
    if request.method == 'POST':
        no_file = jsonify(
            error='No file',
            message='No File has been selected'
        )
        # No file in the requist
        if 'file' not in request.files:
            return no_file, 500
        # Get the file nama
        file = request.files['file']

        if file.filename == '':
            return no_file, 500
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            saving_file = os.path.join(APP.config['UPLOAD_FOLDER'], filename)
            file.save(saving_file)
            # Read the image size
            img = cv2.imread(saving_file)
            height, width = img.shape[:2]
            image = Image(name=filename, width=width, height=height)
            db.session.add(image)
            try:
                db.session.commit()
            except exc.SQLAlchemyError as e:
                db.session.rollback()
            # Return a page object
            image_meta = get_image_by_name(filename)
            return jsonify(image_meta), 201


@IMAGE.route('<path:file_name>')
def get_image(file_name):
    base_path = os.path.join(os.getcwd(), APP.config['UPLOAD_FOLDER'])
    return send_from_directory(base_path, file_name)


@IMAGE.route('section/<path:file_name>', methods=['POST'])
def section_identification(file_name):
    if request.method == 'POST':
        crop_image(request.get_json(), file_name)
    return ""


def get_image_by_name(filename):
    try:
        image_db = Image.query.filter_by(name=filename).first()
    except:
        print('Error on getting name')
    image_file = os.path.join('image', filename)
    image = {
        "url": "{}/{}/{}".format(APP.config['HOST'], APP.config['BASE_PATH'], image_file),
        "name": image_db.name,
        "width": image_db.width,
        "height": image_db.height
    }
    return image


def allowed_file(filename):
    """ List of allowed file formats"""
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
