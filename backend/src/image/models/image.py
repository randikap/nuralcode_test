""" Doc module """
from src import DB as db


class Image(db.Model):
    """ Image class """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255),unique=True)
    width = db.Column(db.Integer)
    height = db.Column(db.Integer)

    def __init__(self, name, width, height):
        self.name = name
        self.width = width
        self.height = height

    def __repr__(self):
        return '<Image %r: width:%r height:%r >' % (self.name, self.width, self.height)
