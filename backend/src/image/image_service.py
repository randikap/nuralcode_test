import os
import cv2
from flask import current_app as APP
from .classify import calasify

MODEL_NAME = 'frozen_inference_graph.pb'
CWD_PATH = os.getcwd()
MODEL_LOCATION = os.path.join(CWD_PATH, 'models',  MODEL_NAME)


def crop_image(image, file_name):
    base_path = os.path.join(os.getcwd(), APP.config['UPLOAD_FOLDER'])
    image_path = os.path.join(base_path, file_name)
    img = cv2.imread(image_path)

    y_min = int(image['y'])
    y_max = int(image['height'])
    x_min = int(image['x'])
    x_max = int(image['width'])
    print(y_min, y_max, x_min, x_max)

    crop = img[y_min:y_min+y_max, x_min:x_min+x_max]
    file = 'out/out.png'
    # test_image = cv2.imread('out/imgpsh_fullsize.jpeg')
    cv2.imwrite(file, crop)
    calasify(crop)
