import os
import numpy as np
import logging
from keras.models import Sequential, load_model
from .resize import resize
from keras.preprocessing import image

PREVIOUS_WEIGHTS = os.path.join(
    os.getcwd(), 'weights', "mobi_keras_final_model.h5")
IMAGE_RESIZE_HEIGHT = int(224)
IMAGE_RESIZE_WIDTH = int(224)


def get_model():
    print("Path to model {}".format(PREVIOUS_WEIGHTS))
    global model
    model = load_model(PREVIOUS_WEIGHTS)
    print('Model loaded')


get_model()


def calasify(img):
    # test_image = image.load_img('weights/imgpsh_fullsize.jpeg', target_size=(224, 224))
    # test_image = image.img_to_array(test_image)
    # test_image = np.expand_dims(test_image, axis=0)
    # result = model.predict(test_image, batch_size=1)
    # k = result[0]
    # print(k)

    # resized_image = cv2.resize(crop_img,(int(224),int(224)))
    resized_image = resize(img, IMAGE_RESIZE_HEIGHT, IMAGE_RESIZE_WIDTH)
    test_image = np.expand_dims(resized_image, axis=0)
    result = model.predict(test_image, batch_size=1)

    j = np.argmax(result[0], axis=-1)

    if j == 0:
        element_name = "contact"
    elif j == 1:
        element_name = "cover_image"
    elif j == 2:
        element_name = "footer"
    elif j == 3:
        element_name = "grid"
    elif j == 4:
        element_name = "jumbotron"
    elif j == 5:
        element_name = "maps"
    elif j == 6:
        element_name = "toolbar"

    logging.info(
        "classified element in "
        + element_name
        + " with a precision of "
        + str(result[0][j])
    )

    return_values = [element_name, result[0][j]]
    print(return_values)

    return return_values
