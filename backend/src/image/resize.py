"""Doc """
import numpy as np
import cv2


def resize(image, width, height):
    dim = None
    # image = cv2.imread(img)
    h, w = image.shape[:2]

    if width is None and height is None:
        return image

    x_off = 0
    y_off = 0

    if h > w:
        r = height / float(h)
        dim = (int(w * r), height)

    else:
        r = width / float(w)
        dim = (width, int(h * r))

    resized = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)
    (h_0, w_0) = resized.shape[:2]
    avg_color_per_row = np.average(resized, axis=0)
    avg_color = np.average(avg_color_per_row, axis=0)
    color_num = (avg_color[0] + avg_color[1] + avg_color[2])/3

    if h_0 > w_0:
        x_off = int((224 - w_0)/2)
    else:
        y_off = int((224 - h_0)/2)

    blank_image = np.zeros((height, width, 3), np.uint8)
    blank_image.fill(color_num)

    blank_image[y_off:y_off+h_0, x_off:x_off+w_0] = resized
    return blank_image
