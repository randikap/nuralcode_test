""" Doc """
from src import APP


if __name__ == '__main__':
    # APP.run()
    from gevent import pywsgi
    from geventwebsocket.handler import WebSocketHandler
    server = pywsgi.WSGIServer(('', 5000), APP, handler_class=WebSocketHandler)
    APP.debug = True
    server.serve_forever()
