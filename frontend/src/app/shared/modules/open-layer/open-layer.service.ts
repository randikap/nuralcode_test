import { Injectable, NgZone } from '@angular/core';
import * as ol from 'openlayers';
import { Observable, Observer } from 'rxjs';
import { Image } from './models/image';

@Injectable()
export class OpenLayerService {
  public map: ol.Map;
  private projection: ol.proj.Projection;
  private imageLayer: ol.layer.Image;
  private extent: ol.Extent;

  public featureIds = [];

  // Draw thte boxes
  public boxFeatureSource: ol.source.Vector;
  public boxFeatureVector: ol.layer.Vector = new ol.layer.Vector({
    source: this.boxFeatureSource
  });

  constructor(private zone: NgZone) {
    this.projection = new ol.proj.Projection({
      code: 'xkcd-image',
      units: 'pixels'
    });

    this.boxFeatureVector.setZIndex(2);

    this.map = new ol.Map({
      controls: [],
      layers: [this.boxFeatureVector]
    });
  } // constructor

  public init(htmlElement: HTMLElement) {
    this.map.setTarget(htmlElement);
  }

  public setImage(image: Image): void {
    // Create extent and projection
    let imageExtent: ol.Extent = [0, 0, image.width, image.height];
    let projection = Object.assign(this.projection);
    // console.log(`Image extent ${imageExtent}`);
    projection.setExtent(imageExtent);
    // console.log(`Image projection ${projection.getExtent()}`);
    // Create image layer
    let imageLayer = new ol.layer.Image({
      source: new ol.source.ImageStatic({
        url: image.url,
        projection,
        imageExtent
      })
    });
    // view
    let view = new ol.View({
      projection,
      center: ol.extent.getCenter(imageExtent),
      zoom: 1.8,
      maxZoom: 8
    });

    // Features
    // Box feature
    let boxFeatureSource = new ol.source.Vector({ wrapX: true });

    // Remove previous layers
    this.map.removeLayer(this.imageLayer);
    this.boxFeatureVector.setSource(boxFeatureSource);

    // Add new Layer
    this.map.addLayer(imageLayer);
    //  this.map.addLayer(this.boxFeatureVector)

    // Set global variables to use within other components
    this.imageLayer = imageLayer;
    this.extent = imageExtent;
    this.boxFeatureSource = boxFeatureSource;
    this.map.setView(view);
  }

  private getLastFeature() {
    if (this.featureIds.length > 0) {
      let lastId = this.featureIds[this.featureIds.length - 1];
      return this.boxFeatureSource.getFeatureById(lastId);
    }
    return null;
  } // getLastFeature()

  public drawBoxFeature(e: ol.MapBrowserEvent): ol.Coordinate[] {
    let c = e.coordinate;
    let lastFeature = this.getLastFeature();
    let imageHeight = this.extent[3];
    let imageWidth = this.extent[2];
    let cBox: ol.Coordinate[];
    if (lastFeature) {
      let poly = <ol.geom.Polygon>lastFeature.getGeometry();
      let ex = poly.getCoordinates()[0];
      let h = ex[2][1];
      cBox = [
        // Top left
        [0, h],
        // Top right
        [imageWidth, h],
        // Bottom right
        [imageWidth, c[1]],
        // Bottom left
        [0, c[1]]
      ];
    } else {
      cBox = [
        // Top left
        [0, imageHeight],
        // Top right
        [imageWidth, imageHeight],
        // Bottom right
        [imageWidth, c[1]],
        // Bottom left
        [0, c[1]]
      ];
    }
    // Draw the box
    let featureBox = new ol.Feature({
      geometry: new ol.geom.Polygon([cBox])
    });

    let s = new ol.style.Style({
      fill: new ol.style.Fill({
        color: 'rgba(0,0,0,0.8)'
      }),
      text: new ol.style.Text({
        font: '20px Roboto-Regular',
        fill: new ol.style.Fill({
          color: '#fff'
        })
      }),
      stroke: new ol.style.Stroke({ color: '#64a5fd', width: 1 })
    });

    featureBox.setStyle(s);
    let id = this.uuidv4();
    this.featureIds.push(id);
    featureBox.setId(id);

    this.boxFeatureSource.addFeature(featureBox);
    return cBox;
  } //drawBoxFeature()

  public uuidv4(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (Math.random() * 16) | 0,
        v = c == 'x' ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  } //uuidv4()

  public subscriobeToMapEvent<E>(eventName: string): Observable<E> {
    return Observable.create((observer: Observer<ol.MapBrowserEvent>) => {
      this.map.on(eventName, (event: ol.MapBrowserEvent) =>
        this.zone.run(() => observer.next(event))
      );
    });
  } // subscriobeToMapEvent()
}
