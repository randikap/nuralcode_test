import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter
} from '@angular/core';
import { OpenLayerService } from './open-layer.service';
import { Image } from './models/image';
import { MapBrowserEvent } from 'openlayers';
import { log } from 'util';

@Component({
  selector: 'app-open-layer',
  templateUrl: './open-layer.component.html',
  styleUrls: ['./open-layer.component.scss']
})
export class OpenLayerComponent implements OnInit, OnChanges {
  @ViewChild('canvas')
  public canvas: ElementRef;

  @Input()
  public image: Image;

  @Output()
  public section: EventEmitter<ol.Coordinate[]> = new EventEmitter<ol.Coordinate[]>();

  constructor(private openLayerService: OpenLayerService) {}

  ngOnInit() {
    this.openLayerService.init(this.canvas.nativeElement);
    this.openLayerService.subscriobeToMapEvent('click').subscribe((e: MapBrowserEvent) => {
      let coords = this.openLayerService.drawBoxFeature(e);
      this.section.emit(coords);
    });
  } // ngOnInit()

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.image && changes.image.currentValue) {
      console.debug(`Image changed ${this.image.url}`);
      this.openLayerService.setImage(this.image);
    }
  } // ngOnChanges()
} // class
