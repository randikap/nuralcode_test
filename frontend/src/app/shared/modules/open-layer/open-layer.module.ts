import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import {
  MatToolbarModule,
  MatButtonModule,
  MatIconModule
} from "@angular/material";
import { OpenLayerComponent } from "./open-layer.component";
import { OpenLayerService } from "./open-layer.service";
import { FlexLayoutModule } from "@angular/flex-layout";

@NgModule({
  imports: [
    CommonModule,
    // FlexLayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule
  ],
  declarations: [OpenLayerComponent],
  exports: [OpenLayerComponent],
  providers: [OpenLayerService]
})
export class OpenLayerModule {}
