import { TestBed } from '@angular/core/testing';

import { OpenLayerService } from './open-layer.service';

describe('OpenLayerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OpenLayerService = TestBed.get(OpenLayerService);
    expect(service).toBeTruthy();
  });
});
