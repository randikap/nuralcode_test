export class Image {
  url: string;
  id?: number;
  name?: string;
  width?: number;
  height?: number;
}
