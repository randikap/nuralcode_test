import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CanvasRoutingModule } from "./canvas-routing.module";
import { CanvasComponent } from "./canvas.component";
import { OpenLayerModule } from "src/app/shared/modules/open-layer/open-layer.module";
import { MatButtonModule, MatIconModule, MatSnackBarModule } from "@angular/material";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    CanvasRoutingModule,
    OpenLayerModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    MatSnackBarModule
  ],
  declarations: [CanvasComponent]
})
export class CanvasModule {}
