import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { throwError } from 'rxjs';
import { catchError, take } from 'rxjs/operators';
import { Image } from 'src/app/shared/modules/open-layer/models/image';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements OnInit {
  public image: Image;

  public loading: boolean = false;

  public formGroup = this.fb.group({
    file: [null, Validators.required],
    fileName: []
  });

  constructor(private http: HttpClient, private fb: FormBuilder, private snackBar: MatSnackBar) {
    // this.image = {
    //   url: 'https://extendz.github.io/extendz-live/assets/img/bg.jpg',
    //   width: 960,
    //   height: 596
    // };
  } // constructor

  ngOnInit() {}

  public onFileChange(event) {
    this.onSubmit(event.target.files);
  } // onFileChange()

  public onSection(box) {
    let height = box[1][1] - box[2][1];
    let width = box[1][0] - box[0][0];
    let x = box[0][0];
    let y = this.image.height - box[0][1];
    let section = { width, height, x, y };
    this.http.post(`image/section/${this.image.name}`, section).subscribe();
  } //onSection()

  public onSubmit(files) {
    this.loading = true;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data'
      })
    };
    let fd = new FormData();
    fd.append('file', files[0], files[0].name);
    this.http
      .post('image/upload', fd)
      .pipe(
        take(1),
        catchError((err: HttpErrorResponse) => {
          this.snackBar.open(err.message, null, { duration: 5000 });
          return throwError(err);
        })
      )
      .subscribe((image: Image) => {
        this.image = image;
        this.loading = false;
      });
  } // onSubmit()
}
